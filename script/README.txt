Ghalatawi: Arabic AUtocorrect,
Files:
=======
test_ghalatawi.py : read a file, and return words with proposed autocorrection.
Example 
	python  test_ghalatawi.py -f samples/test.txt >output/output.txt
core:
	python file sources
	ar_ghalat.py contains autocorrect functions
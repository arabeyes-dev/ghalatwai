#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys
import re
import string
import datetime
import  getopt
import os
from core.ar_ghalat import *

token_pat=re.compile(u"[^\w\u064b-\u0652']+",re.UNICODE);
def tokenize(text=u""):
	"""
	Tokenize text into words
	@param text the input text.
	@type text: unicode.
	@return: list of words.
	@rtype: list.
	"""

	if text==u'':
		return [];
	else:
		mylist= token_pat.split(text)
		if u'' in mylist: mylist.remove(u'');
		return mylist;
		
def validate_word_list(word_list, dWordlist,show_correct=False):
	for word in word_list:
		word=araby.stripTatweel(word);
		valid=araby.isArabicword(word);
		if not valid:
			print (u"\t".join([word,str(valid)])).encode('utf8');

		result=autocorrectByWordlist(word, dWordlist);
		if result:
			print u"\t#".join([word,result]).encode('utf-8');		
		else: # if the word isn't in the autolist, use regular expression
			result=autocorrectByRegex(word);
			if result:
				print u"\t*".join([word,result]).encode('utf-8');
			else:
				if show_correct:
					print u"\t".join([word]).encode('utf-8');


FROM_FILE=True;
LIMIT=1000000
scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	fname = ''
	suggestion=False;
	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVdrtsv:f:",
                               ["help", "version","dict", "root", "template","all",
                                "seg", "vocalized", "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-s", "--seg"):
			suggestion = True
		if o in ("-f", "--file"):
			fname = val

	return (fname,suggestion)

def main():
	filename,suggestion=grabargs()
	print "file name ",filename
	#load autocorrect list
	load_autocorrectlist("data/arabic.acl")
	#ToDo1 ref
	if FROM_FILE:
		try:
			fl=open(filename);
		except:
			print "can't open the file",
			exit();
		#readslines
		line=fl.readline().decode("utf8");
		counter=0;
		while line :
			line=chomp(line)
			if not line.startswith("#"):
				#treat a line
				word_list=tokenize(line);
				validate_word_list(word_list,True);
				print counter;
			line=fl.readline().decode("utf8");
			counter+=1;
		fl.close();
	else:
		word_list=(u"إستعداء",
		)
		validate_word_list(word_list[:LIMIT],True);



if __name__ == "__main__":
  main()








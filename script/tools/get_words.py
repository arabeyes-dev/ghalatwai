﻿#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys
import re
import string
import datetime
import  getopt
import os
from core.ar_ghalat import *

FROM_FILE=True;
LIMIT=1000
scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	fname = ''
	suggestion=False;
	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVdrtsv:f:",
                               ["help", "version","dict", "root", "template","all",
                                "seg", "vocalized", "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-s", "--seg"):
			suggestion = True
		if o in ("-f", "--file"):
			fname = val

	return (fname,suggestion)

def main():
	filename,suggestion=grabargs()
	print "file name ",filename
	#ToDo1 ref
	if FROM_FILE:
		try:
			fl=open(filename)
		except:
			sys;exit();
	line=fl.readline().decode("utf8");

	while line :
		if not line.startswith("#"):
			#text=text+" "+chomp(line)
			line=line.strip('\n')
			liste=line.split('\t');
			if len(liste)>=1:
				print liste[0].encode("utf8");
		line=fl.readline().decode("utf");
	fl.close();


if __name__ == "__main__":
  main()







